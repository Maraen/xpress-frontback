import React, { useState, useEffect } from "react";

export default props => {
  const { postId } = props.match.params;

  const [dataState, updateData] = useState([]); //Hook state

  useEffect(() => {
    fetch("http://localhost:3000/posts")
      .then(response => response.json())
      .then(data => updateData(data[postId - 1]))
      .catch(function(error) {
        console.log("Houston we have a problem");
      });
  });
  return (
    <div>
      <h1>Id: {postId}</h1>
      {dataState.name}
      {dataState.context}
      <br />
      post created {dataState.date}
      <div />
    </div>
  );
};
