import React, { Component } from "react";
import { BrowserRouter as Link } from "react-router-dom";

export default class Posts extends Component {
  componentDidMount() {
    fetch("http://localhost:3000/posts")
      .then(response => response.json())
      .then(data => {
        this.setState({ data: data });
      })
      .catch(function(error) {
        console.log("Houston we have a problem");
      });
  }

  render() {
    if (this.state === null) {
      return <div>Loading</div>;
    } else {
      return (
        <div>
          {/* <Router> */}
          {this.state.data &&
            this.state.data.map((post, index) => (
              <div key={index}>
                <Link to={`/posts/${post.id}`}>
                  {post.id} {post.name} {post.context} {post.date}
                </Link>
              </div>
            ))}
          {/* </Router> */}
        </div>
      );
    }
  }
}
