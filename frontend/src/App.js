import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Posts from "./components/Posts";
import PostPage from "./components/PagePost";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Posts} />
          <Route exact path="/posts/:postId" component={PostPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
