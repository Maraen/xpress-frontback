const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const posts = require("./posts.json");
// Configuration de Express
const PORT = 3000;
app.use(morgan("combined "));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Déclaration des routes
app.get("/posts", (req, res) => {
  res.json(posts);
});
app.get("/posts/:id", (req, res) => {
  const { id } = req.params;
  const post = posts.find(p => p.id == id);
  res.json(post);
});
app.post("/posts", (req, res) => {
  const post = req.body;
  posts.push(post);
  res.json(posts);
});
app.delete("/posts/:id", (req, res) => {
  const { id } = req.params;
  posts = posts.find(p => p.id !== id);
  res.json(`Post ${id} deleted`);
});
app.put("/posts/:id", (req, res) => {
  const { id } = req.params;
  const updatedPost = req.body;
  posts = posts.map(post => (id === post.id ? updatedPost : post));
  res.json(updatedPost);
});

app.use(express.static("./frontend/build")); // can use this if you want to run only on 1 port (however you'll have to rebuild after every save)
// Lancement du serveur Web
app.listen(PORT, () => console.log(`Serveur lancé sur le port ${3000}`));
